﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadingLight : MonoBehaviour {
	public Color c1 = Color.red; 
	public Color c2 = Color.green; 
	public Color c3 = Color.blue; 
	public Light fadingLight; 
	public float duration = 1.0F; 
	// Use this for initialization
	void Start () {
		fadingLight = GetComponent<Light>();
		fadingLight.color = c1;
	}
	
	// Update is called once per frame
	void Update () {
		float t = Mathf.PingPong(Time.time, duration) / 2*duration;
		fadingLight.color = Color.Lerp (c1, c2, t);
	}
}
